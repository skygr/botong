<?php

//Path Definition
Frame::addModelPath(__DIR__.'/Backend/Models');
Frame::addViewPath(__DIR__.'/Frontend/Views');
Frame::addViewEnginePath(__DIR__.'/Backend/ViewEngines');
Frame::addControllerPath(__DIR__.'/Backend/Controllers');

Frame::addRouterRule('Src', 'Css', '/Css/{FileName}');
Frame::addRouterRule('Src', 'Js', '/Js/{FileName}');
Frame::addRouterRule('Src', 'Font', '/Font/{FileName}');
Frame::addRouterRule('Src', 'Image', '/Image/{FileName}');

Frame::addRouterRule('Home', 'Index', '/');
Frame::addRouterRule('Home', 'Reg', '/Reg');
Frame::addRouterRule('Home', 'Login', '/Login');
Frame::addRouterRule('Home', 'Zcxy', '/Zcxy');
Frame::addRouterRule('Home', 'Zcxy', '/Zcxy/{CategoryId}');

Frame::addRouterRule('Ajax', 'Reg', '/Ajax/Reg');
Frame::addRouterRule('Ajax', 'Login', '/Ajax/Login');
Frame::addRouterRule('Ajax', 'Logout', '/Ajax/Logout');
Frame::addRouterRule('Ajax', 'GetComment', '/Ajax/GetComment/{Offset}/{Limit}');
Frame::addRouterRule('Ajax', 'AppendComment', '/Ajax/AppendComment');
Frame::addRouterRule('Ajax', 'GetCategory', '/Ajax/GetCategory');

Frame::addRouterRule('Admin', 'Login', '/Admin/Login');
Frame::addRouterRule('Admin', 'Console', '/Admin/Console');
Frame::addRouterRule('Admin', 'Console', '/Admin');
Frame::addRouterRule('Admin', 'Zcxy', '/Admin/Zcxy');
Frame::addRouterRule('Admin', 'AjaxLogin', '/Admin/Ajax/Login');
Frame::addRouterRule('Admin', 'AjaxLogout', '/Admin/Ajax/Logout');
Frame::addRouterRule('Admin', 'AjaxAppendCategory', '/Admin/Ajax/AppendCategory');
Frame::addRouterRule('Admin', 'AjaxRemoveCategory', '/Admin/Ajax/RemoveCategory/{Id}');

Frame::addRouterRule('Frontend', 'View', '/Frontend/{filename}');

Frame::addRouterRule('Debug', 'Ip', '/Ip');

