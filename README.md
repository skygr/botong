# 标博士 - Botong.com

## 面向前端开发人员的说明
* 视图传入变量的获取?

		<?php
			$this->变量名;
			...
		?>
		
* 视图生成指向控制器的Url?
		
		<?php
			echo $this->getUrl('控制器名', '方法名', array('参数' => 值,...));
		?>


## 视图列表

### 首页
* 视图: `Home`
* 视图传入参数:
	* 用户名
		* 变量: `$this->Username`
		* 类型: `String | Bool(false)`
		* 描述: `Session`里记录的用户名，未登录是为`false`
	* 城市列表
		* 变量: `$this->AreaMenu`
		* 类型: `Array('拼音首字母' => Array(String), ...)`
		* 描述: 城市切换到的列表
	* 推荐城市
		* 变量: `$this->AreaSuggest`
		* 类型:  `String`
		* 描述: 根据IP自动定位的城市
	* ...

### 用户-登录
* 视图: `Login`
* 视图传入参数: `empty`

### 用户-注册
* 视图: `Reg`
* 视图传入参数: `empty`

### 前台-知产学院
* 视图: `Zcxy`
* 视图传入参数:
	* 用户名
		* 变量: `$this->Username`
		* 类型: `String | Bool(false)`
		* 描述: `Session`里记录的用户名，未登录是为`false`
	* 城市列表
		* 变量: `$this->AreaMenu`
		* 类型: `Array('拼音首字母' => Array(String), ...)`
		* 描述: 城市切换到的列表
	* 推荐城市
		* 变量: `$this->AreaSuggest`
		* 类型:  `String`
		* 描述: 根据IP自动定位的城市

### 后台-登录
* 视图: `Admin-Login`
* 视图传入参数: `empty`

### 后台-面板
* 视图: `Admin-Console`
* 视图传入参数: `empty`

### 后台-知产学院
* 视图: `Admin-Zcxy`
* 视图传入参数: `empty`

## 控制器列表

### 静态资源加载
* 控制器: `Src`
* 方法清单:
	* 样式表加载
		* 方法: `Css`
		* 参数:
			* `FileName`: 文件名
		* 描述:
			* 返回`Frontend/Views/Css/{$FileName}`
	* 脚本加载
		* 方法: `Js`
		* 参数:
			* `FileName`: 文件名
		* 描述:
			* 返回`Frontend/Views/Js/{$FileName}`
	* 图片加载
		* 方法: `Image`
		* 参数:
			* `FileName`: 文件名
		* 描述:
			* 返回`Frontend/Views/Image/{$FileName}`
	* 字体加载
		* 方法: `Font`
		* 参数:
			* `FileName`: 文件名
		* 描述:
			* 返回`Frontend/Views/Font/{$FileName}`

### Ajax
* 控制器: `Ajax`
* 方法清单:
	* 注册
		* 方法: `Reg`
		* 路由:
			* `/Ajax/Reg`
		* POST:
			* `Username`: 用户名
			* `Password`: 密码
		* 描述:
			* 用户注册
	* 登录
		* 方法: `Login`
		* 路由: `/Ajax/Login`
		* POST:
			* `Username`: 用户名
			* `Password`: 密码
		* 描述:
			* 用户登录
	* 退出
		* 方法: `Logout`
		* 路由: `/Ajax/Logout`
		* 描述:
			* 用户退出
	* 获取首页评论
		* 方法: `GetComment`
		* 路由: 
			* `/Ajax/GetComment/{Offset}/{Limit}`
			* 参数:
				* `Offset`: 偏移量
				* `Limit`: 获取量
		* 描述:
			* 获取首页评论
	* 发布首页评论
		* 方法: `AppendComment`
		* 路由: `/Ajax/AppendCommend`
		* POST:
			* `Content`: 评论正文
		* 描述:
			* 发布评论
	* 获取新闻列表（知产学院）
		* 方法: `GetCategory`
		* 路由: `/Ajax/GetCategory`
		* 描述:
			* 获取新闻列表（知产学院）

### 前台相关
* 控制器: `Home`
* 方法清单:
	* 首页
		* 方法: `Index`
		* 路由: `/`
		* 描述: 首页
	* 注册
		* 方法: `Reg`
		* 路由: `/Reg`
		* 描述: 注册页
	* 登录
		* 方法: `Login`
		* 路由: `/Login`
		* 描述: 登陆页
	* 知产学院
		* 方法: `Zcxy`
		* 路由: `/Zcxy`, `/Zcxy/{CategoryId}`
		* 描述: 新闻(知产学院)

### 后台相关
* 控制器: `Admin`
* 方法清单:
	* 登录页面
		* 方法: `Login`
		* 路由: `/Admin/Login`
		* 描述:
			* 后台登录
	* 控制台
		* 方法: `Console`
		* 路由: `/Admin/Console`, `/Admin`
		* 描述:
			* 后台控制页面（非管理员登录状态会跳转到`/Admin/Login`）
	* 知产学院
		* 方法: `Zcxy`
		* 路由: `/Admin/Zcxy`
		* 描述:
			* 后台-知产学院
	* 登录
		* 方法: `AjaxLogin`
		* 路由: `/Admin/Ajax/Login`
		* POST:
			* `Username`: 管理员用户名
			* `Password`: 管理员密码
		* 描述:
			* 发起管理员登录动作，保持会话状态.
	* 退出
		* 方法: `AjaxLogout`
		* 路由: `/Admin/Ajax/Logout`
		* 描述:
			* 发起管理员退出动作，去掉会话.
	* 修改密码
		* 方法: `AjaxSetPassword`
		* 路由: `/Admin/Ajax/SetPassword`
		* POST:
			* `Password`: 密码
		* 描述:
			* 修改当前登录管理员的密码。
	* 删除首页评论
		* 方法: `AjaxRemoveComment`
		* 路由: `/Admin/Ajax/RemoveComment/{Id}`
			* `Id`: 评论标识(在`/Ajax/GetComment`中获取)
		* 描述:
			* 删除指定评论
	* 重命名目录(知产学院)
		* 方法: `AjaxRenameCategoy`
		* 路由: `/Admin/Ajax/RenameCategory/{Id}`
			* `Id`: 目录标识
		* POST:
			`Name`: 新的目录名
		* 描述:
			重命名目录
	* 添加目录(知产学院)
		* 方法: `AjaxAppendCategory`
		* 路由: `/Admin/Ajax/AppendCategory`
		* POST:
			* `Name`: 目录名
		* 描述:
			* 添加目录
	* 删除目录（知产学院）
		* 方法: `AjaxRemoveCategory`
		* 路由: `/Admin/Ajax/RemoveCategory/{Id}`
			* `Id`: 目录标识(在`/Ajax/GetCategory`中获取)
		* 描述:
			* 删除指定目录
	

### 视图加载测试
* 控制器: `Frontend`
* 方法清单:
	* 视图加载
		* 方法: `View`
		* 参数:
			* `FileName`: 视图名
		* 描述:
			* 返回`Frontend/Views/{$FileName}.php`

## 模型列表

### 数据库加载
* 模型: `Db`
* 方法清单:
	* 数据库查询
		* 方法: `Query`
		* 参数:
			* `Sql`: SQL语句
		* 描述:
			* 对于`mysqli_query`的封装
	* 字段安全过滤
		* 方法: `Escape`
		* 参数:
			* `Var`: SQL字段的值
		* 描述:
			* 对于`mysqli_real_escape_string`的封装

### 基于IP的定位
* 模型: `IpAddress`
* 方法清单:
	* 从文件更新IP地址数据库
		* 方法: `LoadFromFile`
		* 参数:
			* `FileName`: 文件名
		* 描述:
			* 从文本文件中更新数据库`IpAddress`表
	* 清空数据
		* 方法: `Drop`
		* 参数: 无
		* 描述:
			* 移除表`IpAddress`
	* 通过IP地址获取地区和机构
		* 方法: `GetAreaCompanyByIp`
		* 参数:
			* `Ip`: IP地址
		* 描述:
			* 通过IP地址获取地区和机构信息
		* 返回值:
			* `list($Area, $Company) || Bool(false)`
	* 根据当前HTTP请求自动获取地区和机构
		* 方法: `GetAreaCompanyAuto`
		* 参数: 无
		* 描述:
			* 通过当前请求的IP地址获取地区和机构信息
		* 返回值:
			* `list($Area, $Company) || Bool(false)`
	* 获取当前HTTP请求的IP
		* 方法: `GetIp`
		* 参数: 无
		* 描述:
			* 获取当前请求者IP
		* 返回值:
			* `String($Ip) || Bool(false)`

### 汉字拼音转换
* 模型: `Pinyin`
* 方法清单:
	* 从文件加载拼音数据
		* 方法: `LoadFromFile`
		* 参数:
			* `FileName`: 文件名
		* 描述:
			* 从文件获取拼音数据更新到Pinyin
	* 清空数据
		* 方法: `Drop`
		* 参数: 无
		* 描述:
			* 移除表Area
	* 通过汉字获取拼音
		* 方法: `GetPinyinByChar`
		* 参数:
			* `Char`: 一个汉字
		* 描述:
			* 汉字转换为拼音

### 地区划分
* 模型: `Area`
* 方法清单:
	* 从文件加载地区数据
		* 方法: `LoadFromFile`
		* 参数:
			* `FileName`: 文件名
		* 描述:
			* 从文件获取地区划分更新到数据库Area
	* 清空数据
		* 方法: `Drop`
		* 参数: 无
		* 描述:
			* 移除表Area
	* 判断是否存在指定地区划分
		* 方法: `Exists`
		* 参数:
			* `Area`: 地区名
		* 描述:
			* 判断是否存在指定的地区划分
		* 返回值:
			* `Boolean`
	* 增加地区
		* 方法: `Append`
		* 参数:
			* `Area`: 地区名
		* 描述:
			* 增加地区划分，模型会自动去除重名
	* 删除地区
		* 方法: `Remove`
		* 参数:
			* `Area`: 地区名
		* 描述:
			* 删除指定地区
	* 获取所有地区
		* 方法: `All`
		* 参数: 无
		* 描述:
			* 获取所有地区
		* 返回值:
			* `Array Of String`
	* 生成按拼音首字母划分的菜单数据
		* 方法: `Menu`
		* 参数: 无
		* 描述:
			* 用户切换地区菜单的数据
		* 返回值:
			* `Array(`拼音首字母` => Array(String), ...)`

### 账号系统
* 模型: `User`
* 方法清单:
	* 用户列表
		* 方法: `ToList`
		* 参数:
			* `Offset`: 偏移
			* `Limit`: 返回量
		* 描述:
			* 获取用户列表
		* 返回值:
			* `Array(Array('id' => String, 'username' => String), ...)`
	* 判断用户是否存在
		* 方法: `Exists`
		* 参数:
			* `Username`: 用户名
		* 描述:
			* 判断用户是否存在
		* 返回值:
			* `Bool`
	* 增加用户
		* 方法: `Append`
		* 参数:
			* `Username`: 用户名
			* `Password`: 密码
		* 描述:
			* 增加用户
	* 删除用户
		* 方法: `Remove`
		* 参数:
			* `Id`: 用户标识
		* 描述:
			* 删除用户
	* 判断用户合法性
		* 方法: `Check`
		* 参数:
			* `Username`: 用户名
			* `Password`: 密码
		* 描述:
			* 检查用户名、密码
		* 返回值:
			* `Int|Bool(false)`
	* 获取指定用户
		* 方法: `Get`
		* 参数:
			* `Id`: 用户标识
		* 描述:
			* 获取指定用户的数据
		* 返回值:
			* `Array of Array('id' => String, 'username' => String)`
	* 修改密码
		* 方法: `SetPassword`
		* 参数:
			* `Id`: 用户标识
			* `Password`: 密码
		* 描述:
			* 修改指定用户的密码

### 管理员系统
* 模型: `Admin`
* 方法清单:
	* 添加账号
		* 方法: `Append`
		* 参数:
			* `Username`: 用户名
			* `Password`: 密码
		* 描述: 添加管理员账号
	* 判断用户合法性
		* 方法: `Check`
		* 参数:
			* `Username`: 用户名
			* `Password`: 密码
		* 描述:
			* 检查用户名、密码
		* 返回值:
			* `Int|Bool(false)`
	* 修改密码
		* 方法: `SetPassword`
		* 参数:
			* `Id`: 用户标识
			* `Password`: 密码
		* 描述:
			* 修改指定用户的密码

### 会话保持
* 模型: `Session`
* 方法清单:
	* 获取用户标识
		* 方法: `GetUserid`
		* 参数: 无
		* 描述: 获取当前用户标识
		* 返回值: `String`
	* 获取用户名
		* 方法: `GetUsername`
		* 参数: 无
		* 描述: 获取当前用户名
		* 返回值: `String|Bool(false)`
	* 登录
		* 方法: `Login`
		* 参数:
			* `Username`: 用户名
			* `Password`: 密码
		* 描述:
			* 登录指定用户保持会话
		* 返回值: `Bool`
	* 退出:
		* 方法: `Logout`
		* 参数: 无
		* 描述: 退出
	* 获取管理员标识
		* 方法: `GetAdminid`
		* 参数: 无
		* 描述: 获取当前登录的管理员标识
		* 返回值: `String`
	* 管理员登录
		* 方法: `AdminLogin`
		* 参数:
			* `Username`: 用户名
			* `Password`: 密码
		* 描述:
			* 登录指定管理员保持会话
		* 返回值: `Bool`
	* 管理员退出:
		* 方法: `AdminLogout`
		* 参数: 无
		* 描述: 管理员退出

### 首页评论
* 模型: `Comment`
* 方法清单:
	* 增加评论
		* 方法: `Append`
		* 参数:
			* `Userid`: 用户标识
			* `Content`: 评论正文
		* 描述:
			* 添加首页评论
	* 删除评论
		* 方法: `Remove`
		* 参数:
			* `Id`: 首页评论标识
		* 描述:
			* 删除首页评论
	* 获取首页评论
		* 方法: `ToList`
		* 参数:
			* `Offset`: 偏移量
			* `Limit`: 获取量
		* 描述:
			* 获取首页评论
		* 返回值:
			* `Array Of Array('id' => String, 'username' => String, 'comment' => String, 'data' => String)`

### 新闻目录(知产学院)
* 模型: `Category`
* 方法清单:
	* 增加目录
		* 方法: `Append`
		* 参数:
			* `Name`: 目录名
		* 描述:
			* 增加目录
	* 删除目录
		* 方法: `Remove`
		* 参数:
			* `Id`: 目录标识
		* 描述:
			* 删除指定目录
	* 修改目录名
		* 方法: `SetName`
		* 参数:
			* `Id`: 目录标识
			* `Name`: 新目录名
		* 描述:
			* 修改目录名
	* 修改封面地址
		* 方法: `SetCover`
		* 参数:
			* `Id`: 目录标识
			* `Cover`: 封面地址
		* 描述:
			* 修改目录封面
	* 获取目录
		* 方法: `toList`
		* 参数: 无
		* 描述:
			* 获取全部目录
		* 返回值:
			`Array of Array('id' => String, 'name' => String, 'cover' => String)`