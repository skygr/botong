<?php

class Frame_ViewEngine_Json extends Frame_ViewEngine {
	public function View($_ViewName, $_ViewVariables) {
		echo json_encode(
				array(
					'desc' => $_ViewName,
					'data' => $_ViewVariables
				)
			);
	}
}
