<?php

class Frame_ViewEngine_Raw extends Frame_ViewEngine {
	public function View($_ViewName, $_ViewVariables) {
		$_Mime = isset($_ViewVariables['Mime']) ? ($_ViewVariables['Mime']) : ('application/octet-stream');
		header("Content-type: {$_Mime}");
		echo($this->getView($_ViewName));
	}
}
