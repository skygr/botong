<?php

class Frame_ViewEngine_Php extends Frame_ViewEngine {
	private $ViewVariables = array();
	public function __get($_VariableName) {
		return isset($this->ViewVariables[$_VariableName]) ? ($this->ViewVariables[$_VariableName]) : (false);
	}
	public function View($_ViewName, $_ViewVariables = array()) {
		$this->ViewVariables = $_ViewVariables;
		include($this->getViewFilename($_ViewName.'.php'));
	}
}
