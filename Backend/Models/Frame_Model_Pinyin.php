<?php

class Frame_Model_Pinyin extends Frame_Model {
	private $Db;
	public function __construct() {
		$this->Db = $this->getModel('Db');
		$this->AutoCheck();
	}
	public function AutoCheck() {
		$this->Db->Query("
			CREATE TABLE IF NOT EXISTS `Pinyin` (
				`id` INT PRIMARY KEY AUTO_INCREMENT,
				`char` VARCHAR(8),
				`pinyin` VARCHAR(32)
			);
		");
	}
	public function LoadFromFile($FileName) {
		$Fp = fopen($FileName, "r");
		for(;$Line = fscanf($Fp, "%s\t%s\n");) {
			list($Char, $Pinyin) = $Line;
			$this->Db->Query("
				INSERT INTO `Pinyin`
					SET
						`char` = '{$Char}',
						`pinyin` = '{$Pinyin}'
					;
			");
		}
	}
	public function Drop() {
		$this->Db->Query("
			DROP TABLE IF EXISTS `Pinyin`;
		");
	}
	public function GetPinyinByChar($_Char) {
		$Char = $this->Db->Escape($_Char);
		$Result = $this->Db->Query("
			SELECT `pinyin` FROM `Pinyin`
				WHERE
					`char` = '{$Char}'
				;
		");
		return ($Object = $Result->fetch_object()) ? ($Object->pinyin) : (false);
	}
}
