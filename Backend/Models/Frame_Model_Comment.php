<?php

class Frame_Model_Comment extends Frame_Model {
	private $Db;
	public function __construct() {
		$this->Db = $this->getModel('Db');
		$this->AutoCheck();
	}
	public function AutoCheck() {
		$this->Db->Query("
			CREATE TABLE IF NOT EXISTS `Comment` (
				`id` INT PRIMARY KEY AUTO_INCREMENT,
				`userid` INT,
				`content` VARCHAR(512),
				`date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
				INDEX (`id`),
				FOREIGN KEY (`userid`) REFERENCES `User`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
				INDEX (`date`)
			);
		");
	}
	public function Append($_Userid, $_Content) {
		$Userid = $this->Db->Escape($_Userid);
		$Content = $this->Db->Escape(htmlspecialchars($_Content));
		$this->Db->Query("
			INSERT
				INTO `Comment`
			SET
				`userid` = '{$Userid}',
				`content` = '${Content}';
		");
	}
	public function Remove($Id) {
		$Id = $this->Db->Escape($Id);
		$this->Db->Query("
			DELETE
				FROM `Comment`
			WHERE
				`id` = '{$Id}';
		");
	}
	public function ToList($_Offset, $_Limit) {
		$Offset = $this->Db->Escape($_Offset);
		$Limit = $this->Db->Escape($_Limit);
		$Result = $this->Db->Query("
			SELECT
				`Comment`.`id`, `User`.`username`, `Comment`.`content`, `Comment`. `date`
			FROM
				`Comment`, `User`
			WHERE
				`User`.`id` = `Comment`.`userid`
			ORDER BY
				`Comment`.`date` DESC
			LIMIT
				{$Limit}
			OFFSET
				{$Offset};
		");
		$Comments = array();
		for(;$Comment = $Result->fetch_object();) {
			array_push($Comments, $Comment);
		}
		return $Comments;
	}
}
