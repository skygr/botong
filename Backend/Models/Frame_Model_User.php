<?php

class Frame_Model_User extends Frame_Model {
	private $Db;
	public function __construct() {
		$this->Db = $this->getModel('Db');
		$this->AutoCheck();
	}
	public function AutoCheck() {
		$this->Db->Query("
			CREATE TABLE IF NOT EXISTS `User` (
				`id` INT PRIMARY KEY AUTO_INCREMENT,
				`username` VARCHAR(32),
				`password` VARCHAR(32),
				INDEX (`id`),
				INDEX (`username`),
				INDEX (`password`)
			);
		");
	}
	public function ToList($_Offset, $_Limit) {
		$Offset = $this->Db->Escape($_Offset);
		$Limit = $this->Db->Escape($_Limit);
		$Result = $this->Db->Query("
			SELECT
				`id`, `username`
			FROM
				`User`
			LIMIT
				{$Limit}
			OFFSET
				{$Offset};
		");
		for($Users = array(); $User = $Result->fetch_object(); ) {
			array_push($Users, $User);
		}
		return $Users;
	}
	public function Exists($_Username) {
		$Username = $this->Db->Escape($_Username);
		$Result = $this->Db->Query("
			SELECT
				COUNT(*) AS `count`
			FROM
				`User`
			WHERE
				`username` = '{$Username}';
		");
		return ($Object = $Result->fetch_object()) ? ($Object->count > 0) : (false);
	}
	public function Append($_Username, $_Password) {
		if($this->Exists($_Username) === false) {
			$Username = $this->Db->Escape($_Username);
			$Password = $this->Db->Escape(md5($_Password));
			$this->Db->Query("
				INSERT
					INTO `User`
				SET
					`username` = '{$Username}',
					`password` = '{$Password}';
			");
		}
	}
	public function Remove($_Id) {
		$Id = $this->Db->Escape($_Id);
		$this->Db->Query("
			DELETE
				FROM `User`
			WHERE
				`id` = '{$Id}';
		");
	}
	public function Check($_Username, $_Password) {
		$Username = $this->Db->Escape($_Username);
		$Password = $this->Db->Escape(md5($_Password));
		$Result = $this->Db->Query("
			SELECT
				`id`
			FROM
				`User`
			WHERE
				`username` = '{$Username}' AND
				`password` = '{$Password}';
		");
		return ($Object = $Result->fetch_object()) ? ($Object->id) : (false);
	}
	public function Get($Id) {
		$Result = $this->Db->Query("
			SELECT
				`id`, `username`
			FROM
				`User`
			WHERE
				`id` = '{$Id}';
		");
		return ($Object = $Result->fetch_object()) ? ($Object) : (false);
	}
	public function SetPassword($_Id, $_Password) {
		$Id = $this->Db->Escape($_Id);
		$Password = $this->Db->Escape(md5($_Password));
		$this->Db->Query("
			UPDATE
				`User`
			SET
				`password` = '{$Password}'
			WHERE
				`id` = '{$Id}';
		");
	}
}
