<?php
class Frame_Model_Category extends Frame_Model {
	private $Db;
	public function __construct() {
		$this->Db = $this->getModel('Db');
		$this->AutoCheck();
	}
	public function AutoCheck() {
		$this->Db->Query("
			CREATE TABLE IF NOT EXISTS `Category` (
				`id` INT PRIMARY KEY AUTO_INCREMENT,
				`name` VARCHAR(128)
			);
		");
	}
	public function Append($_Name) {
		$Name = $this->Db->Escape($_Name);
		$this->Db->Query("
			INSERT INTO `Category` SET
				`name` = '{$Name}';
		");
	}
	public function Remove($_Id) {
		$Id = $this->Db->Escape($_Id);
		$this->Db->Query("
			DELETE FROM `Category` WHERE
				`id` = '{$Id}';
		");
	}
	public function toList() {
		$Result = $this->Db->Query("
			SELECT `id`, `name` FROM `Category`;
		");
		$List = array();
		for(; $Object = $Result->fetch_object();) {
			array_push($List, $Object);
		}
		return $List;
	}
}
