<?php

class Frame_Model_Session extends Frame_Model {
	private $MdUser;
	private $MdAdmin;
	public function __construct() {
		$this->MdUser = $this->getModel('User');
		$this->MdAdmin = $this->getModel('Admin');
		session_start();
	}
	public function GetUserid() {
		return (isset($_SESSION['Userid'])) ? ($_SESSION['Userid']) : (false);
	}
	public function GetUserName() {
		if($User = $this->MdUser->Get($this->GetUserid())) {
			return $User->username;
		} else {
			return false;
		}
	}
	public function Login($Username, $Password) {
		if($Userid = $this->MdUser->Check($Username, $Password)) {
			$_SESSION['Userid'] = $Userid;
			return true;
		} else {
			return false;
		}
	}
	public function Logout() {
		unset($_SESSION['Userid']);
	}
	public function GetAdminid() {
		return (isset($_SESSION['Adminid'])) ? ($_SESSION['Adminid']) : (false);
	}
	public function AdminLogin($Username, $Password) {
		if($Adminid = $this->MdAdmin->Check($Username, $Password)) {
			$_SESSION['Adminid'] = $Adminid;
			return true;
		} else {
			return false;
		}
	}
	public function AdminLogout() {
		unset($_SESSION['Adminid']);
	}
}
