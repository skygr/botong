<?php

class Frame_Model_Area extends Frame_Model {
	private $Db;
	private $IpAr;
	public function __construct() {
		$this->Db = $this->getModel('Db');
		$this->IpAr = $this->getModel('IpAddress');
		$this->AutoCheck();
	}
	private function AutoCheck() {
		$this->Db->Query("
			CREATE TABLE IF NOT EXISTS `Area` (
				`id` INT PRIMARY KEY AUTO_INCREMENT,
				`area` VARCHAR(128)
			);
		");
	}
	public function Drop() {
		$this->Db->Query("
			DROP TABLE IF EXISTS `Area`;
		");
	}
	public function Exists($_Area) {
		$Area = $this->Db->Escape($_Area);
		$Result = $this->Db->Query("
			SELECT `id` FROM `Area`
				WHERE
					`area` = '{$Area}'
				;
		");
		return ($Object = $Result->fetch_object()) ? ($Object->id) : (false);
	}
	public function Append($_Area) {
		$Area = $this->Db->Escape($_Area);
		if($this->Exists($Area) === false) {
			$this->Db->Query("
				INSERT INTO `Area`
					SET
						`area` = '{$Area}'
					;
			");
		}
	}
	public function Remove($_Area) {
		$Area = $this->Db->Escape($_Area);
		$this->Db->Query("
			DELETE FROM `Area`
				WHERE
					`area` = '{$Area}'
				;
		");
	}
	public function Menu() {
		$Result = $this->Db->Query("
			SELECT
				`area`, SUBSTRING(`pinyin`, 1, 1) AS `p`
				FROM
					`Area`, `Pinyin`
				WHERE
					SUBSTRING(`area`, 1, 1) = `char`
				;
		");
		$Menu = array();
		for(;$Object = $Result->fetch_Object();) {
			if(!isset($Menu[$Object->p])) {
				$Menu[$Object->p] = array();
			}
			array_push($Menu[$Object->p], $Object->area);
		}
		return $Menu;
	}
	public function All() {
		$Result = $this->Db->Query("
			SELECT `area` FROM `Area`;
		");
		$Areas = array();
		for(;$Object = $Result->fetch_object();) {
			array_push($Areas, $Object->area);
		}
		return $Areas;
	}	
	public function Suggest() {
		if($IpInfo = $this->IpAr->GetAreaCompanyAuto()) {
			list($Area, $Company) = $IpInfo;
			$Areas = $this->All();
			for($i = 0; $i < count($Areas); $i++) {
				if(strpos($Area, $Areas[$i]) !== false) {
					return $Areas[$i];
				}
			}
		}
		return count($Areas) > 0 ? ($Areas[0]) : (false);	
	}
	public function LoadFromFile($FileName) {
		$Fp = fopen($FileName, "r");
		for(;$Line = fscanf($Fp, "%s\n");) {
			list($Area) = $Line;
			$this->Append($Area);
		}	
	}
}
