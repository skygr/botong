<?php

class Frame_Model_IpAddress extends Frame_Model {
	private $Db;
	public function __construct() {
		$this->Db = $this->getModel('Db');
		$this->AutoCheck();
	}
	private function AutoCheck() {
		$this->Db->Query("
			CREATE TABLE IF NOT EXISTS `IpAddress` (
				`id` INT PRIMARY KEY AUTO_INCREMENT,
				`ip_s_1` INT,
				`ip_s_2` INT,
				`ip_s_3` INT,
				`ip_s_4` INT,
				`ip_t_1` INT,
				`ip_t_2` INT,
				`ip_t_3` INT,
				`ip_t_4` INT,
				`area` VARCHAR(128),
				`company` VARCHAR(128)
			);
		");
	}
	public function Drop() {
		$this->Db->Query("
			DROP TABLE IF EXISTS `IpAddress`;
		");
	}
	public function LoadFromFile($FileName) {
		$Fp = fopen($FileName, "r");
		$Count = 0;
		for(;$Line = fscanf($Fp, "%d.%d.%d.%d\t%d.%d.%d.%d\t%s\t%s\n");) {
			list(
				$ip_s_1, $ip_s_2, $ip_s_3, $ip_s_4,
				$ip_t_1, $ip_t_2, $ip_t_3, $ip_t_4,
				$area, $company
			) = $Line;
			$this->Db->Query("
				INSERT INTO `IpAddress`
					SET
						`ip_s_1` = '{$ip_s_1}',
						`ip_s_2` = '{$ip_s_2}',
						`ip_s_3` = '{$ip_s_3}',
						`ip_s_4` = '{$ip_s_4}',
						`ip_t_1` = '{$ip_t_1}',
						`ip_t_2` = '{$ip_t_2}',
						`ip_t_3` = '{$ip_t_3}',
						`ip_t_4` = '{$ip_t_4}'	,
						`area` = '{$area}',
						`company` = '{$company}'
					;
			");
			if((++$Count % 100) === 0)
				printf("%d Records Inserted.\n", $Count);
		}
	}
	public function GetAreaCompanyByIp($Ip) {
		list($ip_1, $ip_2, $ip_3, $ip_4) = sscanf($Ip, "%d.%d.%d.%d");
		$ip_o = ((($ip_1 * 256 + $ip_2) * 256 + $ip_3) * 256 + $ip_4);
		$Result = $this->Db->Query("
			SELECT `area`, `company` FROM `IpAddress`
				WHERE
					(((`ip_s_1` * 256 + `ip_s_2`) * 256 + `ip_s_3`) * 256 + `ip_s_4`) <= {$ip_o} AND
					{$ip_o} <= (((`ip_t_1` * 256 + `ip_t_2`) * 256 + `ip_t_3`) * 256 + `ip_t_4`)
				ORDER BY `id` DESC
				LIMIT 1
			;
		");
		return ($Object = $Result->fetch_object()) ? (Array($Object->area, $Object->company)) : (false);
	}
	public function GetIp() {

		return isset($_SERVER['HTTP_CLIENT_IP']) ?
					($_SERVER['HTTP_CLIENT_IP']) :
					(
						isset($_SERVER['HTTP_X_FORWARDED_FOR']) ?
							($_SERVER['HTTP_X_FORWARD_FOR']) :
							(
								isset($_SERVER['REMOTE_ADDR']) ?
								($_SERVER['REMOTE_ADDR']) :
								(false)
							)
					);
	}
	public function GetAreaCompanyAuto() {
		return $this->GetAreaCompanyByIp($this->GetIp());
	}
}
