<?php
class Frame_Model_Admin extends Frame_Model {
	private $Db;
	public function __construct() {
		$this->Db = $this->getModel('Db');
		$this->AutoCheck();
	}
	public function AutoCheck() {
		$this->Db->Query("
			CREATE TABLE IF NOT EXISTS `Admin` (
				`id` INT PRIMARY KEY AUTO_INCREMENT,
				`username` VARCHAR(32),
				`password` VARCHAR(32),
				INDEX (`id`),
				INDEX (`username`),
				INDEX (`password`)
			);
		");
	}
	public function Append($_Username, $_Password) {
		$Username = $this->Db->Escape($_Username);
		$Password = $this->Db->Escape(md5($_Password));
		$this->Db->Query("
			INSERT INTO `Admin` SET
				`username` = '{$Username}',
				`password` = '{$Password}';
		");
	}
	public function Check($_Username, $_Password) {
		$Username = $this->Db->Escape($_Username);
		$Password = $this->Db->Escape(md5($_Password));
		$Result = $this->Db->Query("
			SELECT `id`
			FROM `Admin`
				WHERE
					`username` = '{$Username}' AND
					`password` = '{$Password}';
		");
		return ($Object = $Result->fetch_object()) ? ($Object->id) : (false);
	}
	public function SetPassword($_Id, $_Password) {
		$Id = $this->Db->Escape($_Id);
		$Password = $this->Db->Escape(md5($_Password));
		$this->Db->Query("
			UPDATE `Admin`
			SET
				`password` = '{$Password}'
			WHERE
				`id` = '{$Id}';
		");
	}
} 
