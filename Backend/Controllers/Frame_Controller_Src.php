<?php

class Frame_Controller_Src extends Frame_Controller {
	private $VeHandle;
	public function __construct() {
		$this->VeHandle = $this->getViewEngine('Raw');
	}
	public function Css() {
		$FileName = $this->Input->getVar('FileName');
		$this->VeHandle->View("Css/{$FileName}", array('Mime' => 'text/css'));
	}
	public function Js() {
		$FileName = $this->Input->getVar('FileName');
		$this->VeHandle->View("Js/{$FileName}", array('Mime' => 'text/javascript'));
	}
	public function Font() {
		$FileName = $this->Input->getVar('FileName');
		$this->VeHandle->View("Font/{$FileName}");
	}
	public function Image() {
		$FileName = $this->Input->getVar('FileName');
		$this->VeHandle->View("Image/{$FileName}");
	}
}
