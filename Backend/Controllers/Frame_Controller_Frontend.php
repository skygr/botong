<?php

class Frame_Controller_Frontend extends Frame_Controller {
	public function View() {
		$filename = $this->Input->getVar('filename');
		$this->getViewEngine('Php')->View($filename);
	}
}
