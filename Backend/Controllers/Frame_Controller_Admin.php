<?php

class Frame_Controller_Admin extends Frame_Controller {
	private $VePhp;
	private $VeJson;
	private $MdSession;
	private $MdAdmin;
	private $MdCategory;
	public function __construct() {
		$this->VePhp = $this->getViewEngine('Php');
		$this->VeJson = $this->getViewEngine('Json');
		$this->MdSession = $this->getModel('Session');
		$this->MdAdmin = $this->getModel('Admin');
		$this->MdCategory = $this->getModel('Category');
	}
	public function Login() {
		$this->VePhp->View('Admin-Login');
	}
	public function Console() {
		if($this->MdSession->GetAdminid()) {
			$this->VePhp->View('Admin-Console');
		} else {
			$this->redirect('Admin', 'Login', array());
		}
	}
	public function Zcxy() {
		if($this->MdSession->GetAdminid()) {
			$this->VePhp->View('Admin-Zcxy');
		} else {
			$this->redirect('Admin', 'Login', array());
		}
	}
	public function AjaxLogin() {
		$Username = $this->Input->getPost('Username');
		$Password = $this->Input->getPost('Password');
		if($this->MdSession->AdminLogin($Username, $Password)) {
			$this->VeJson->View(0, 'Admin login success.');
		} else {
			$this->VeJson->View(1, 'No such admin user or password wrong.');
		}
	}
	public function AjaxLogout() {
		if($this->MdSession->GetAdminid()) {
			$this->MdSession->AdminLogout();
			$this->VeJson->View(0, 'Admin logout success.');
		} else {
			$this->VeJson->View(1, 'You are not login with admin account.');
		}
	}
	public function AjaxAppendCategory() {
		if($this->MdSession->GetAdminid()) {
			$Name = $this->Input->getPost('Name');
			$this->MdCategory->Append($Name);
			$this->VeJson->View(0, 'Append category success.');
		} else {
			$this->VeJson->View(1, 'You are not login with admin account.');
		}	
	}
	public function AjaxRemoveCategory() {
		if($this->MdSession->GetAdminid()) {
			$Id = $this->Input->getVar('Id');
			$this->MdCategory->Remove($Id);
			$this->VeJson->View(0, 'Remove category success.');
		} else {
			$this->VeJson->View(1, 'You are not login with admin accout.');
		}
	}
}
