<?php

class Frame_Controller_Home extends Frame_Controller {
	private $VePhp;
	private $MdSession;
	private $MdArea;
	public function __construct() {
		$this->VePhp = $this->getViewEngine('Php');
		$this->MdSession = $this->getModel('Session');
		$this->MdArea = $this->getModel('Area');
	}
	public function Index() {
		$this->VePhp->View('Home', array(
			"Username" => $this->MdSession->GetUsername(),
			"AreaMenu" => $this->MdArea->Menu(),
			"AreaSuggest" => $this->MdArea->Suggest()
		));			
	}
	public function Reg() {
		$this->VePhp->View('Reg', array());
	}
	public function Login() {
		$this->VePhp->View('Login', array());
	}
	public function Zcxy() {
		$this->VePhp->View('Zcxy', array(
			'Username' => $this->MdSession->GetUsername(),
			'AreaMenu' => $this->MdArea->Menu(),
			'AreaSuggest' => $this->MdArea->Suggest(),
			'CategoryId' => $this->Input->getVar('CategoryId')			
		));
	}
}
