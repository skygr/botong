<?php

class Frame_Controller_Ajax extends Frame_Controller {
	private $VeJson;
	private $MdComment;
	private $MdSession;
	private $MdUser;
	private $MdCategory;
	public function __construct() {
		$this->VeJson = $this->getViewEngine('Json');
		$this->MdComment = $this->getModel('Comment');
		$this->MdSession = $this->getModel('Session');
		$this->MdUser = $this->getModel('User');
		$this->MdCategory = $this->getModel('Category');
		$this->MdAdmin = $this->getModel('Admin');
	}
	public function GetComment() {
		$Offset = $this->Input->getVar('Offset');
		$Limit = $this->Input->getVar('Limit');
		$this->VeJson->View(0, $this->MdComment->ToList($Offset, $Limit));
	}
	public function AppendComment() {
		if($Userid = $this->MdSession->GetUserid()) {
			$Content = $this->Input->getPost('Content');
			$this->MdComment->Append($Userid, $Content);
			$this->VeJson->View(0, 'Append comment success.');
		} else {
			$this->VeJson->View(1, 'Please login first.');
		}
	}
	public function Login() {
		$Username = $this->Input->GetPost('Username');
		$Password = $this->Input->GetPost('Password');
		if($this->MdSession->Login($Username, $Password)) {
			$this->VeJson->View(0, 'Login success.');
		} else {
			$this->VeJson->View(1, 'No such user or password wrong.');
		}
	}
	public function Logout() {
		if($this->MdSession->GetUserid()) {
			$this->MdSession->Logout();
			$this->VeJson->View(0, 'Logout success.');
		} else {
			$this->VeJson->View(1, 'You are not login.');
		}
	}
	public function Reg() {
		$Username = $this->Input->GetPost('Username');
		$Password = $this->Input->GetPost('Password');
		if($this->MdUser->Exists($Username)) {
			$this->VeJson->View(1, 'This username is already exists.');
		} else if(strlen($Password) < 5) {
			$this->VeJson->View(2, 'Password must be more than 5 characters.');
		} else {
			$this->MdUser->Append($Username, $Password);
			$this->VeJson->View(0, 'Reg success.');
		}
	}
	public function GetCategory() {
		$this->VeJson->View(0, $this->MdCategory->toList());
	}
}
