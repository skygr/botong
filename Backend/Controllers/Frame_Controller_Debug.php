<?php

class Frame_Controller_Debug extends Frame_Controller {
	private $IpAr;
	public function __construct() {
		$this->IpAr = $this->getModel('IpAddress');
		$this->Area = $this->getModel('Area');
	}
	public function Ip() {
		header("Content-type: text/html; charset=utf-8");
		printf("IP = %s\n", $this->IpAr->GetIp());
		list($Area, $Company) = $this->IpAr->GetAreaCompanyAuto();
		printf("Area = %s\nCompany = %s\n", $Area, $Company);
		printf("Suggest = %s\n", $this->Area->Suggest());
	}
}
